var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const fs = require('fs')

const { createCanvas, loadImage } = require('canvas')
const canvas = createCanvas(800, 600)
const context = canvas.getContext('2d')
let allClients = new Array();


app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

// Parses geometry data and draws on the canvas
class DrawingTool
{
	constructor(context_)
	{
		this.context = context_;
	}
	
	drawLineStroke(strokeArray)
	{
		// Draw this on a local canvas
		let lastPos = new Object();
		this.context.imageSmoothingQuality = "high"
		this.context.antialias = 'subpixel';
		this.context.lineWidth = 5;
		this.context.lineCap = "round";
		this.context.lineJoin = "round";
		this.context.beginPath();
		for (let i=0; i<strokeArray.length; i++)
		{
			let p = strokeArray[i];
			if (i <= 0) continue;
			
			this.context.moveTo(lastPos.x, lastPos.y);
			this.context.lineTo(p.x, p.y);
			
			lastPos.x = p.x;
			lastPos.y = p.y;
		}
		// Make the line visible
		this.context.stroke();
	}
	
	// Save new canvas in a local .png
	saveCanvasToPNG()
	{
		/*
		const out = fs.createWriteStream(__dirname + '/test.png')
		const stream = canvas.createPNGStream()
		stream.pipe(out)
		out.on('finish', () =>  console.log('The PNG file was created.'))
		*/
	}
}

let dt = new DrawingTool(context);

// Holds a client socket and manages client data exchanges
class RemoteClient
{
	constructor(socket_, context_)
	{
		this.socket = socket_;
		this.context = context_;
		
		// New client
		console.log('RC user connected');
		
		// Bind socket events
		this.socket.on('disconnect', () => { this.handleDisconnection() });
		this.socket.on('input', (msg) => { this.handleLineStroke(msg) });
		
		// Send server canvas
		let imgData = canvas.toDataURL();
		this.socket.emit('servercanvas', imgData);
	}
	
	handleDisconnection()
	{
		console.log('RC user disconnected');
	}
	
	handleLineStroke(msg)
	{
		// TODO : parse data from binary instead of json
		let strokeArray = JSON.parse(msg);
		this.context.drawLineStroke(strokeArray);
		this.socket.broadcast.emit('input', msg);
	}
}

io.on('connection', function(socket)
{
	// Handle new client
	let rc = new RemoteClient(socket, dt);
	allClients.push(rc);
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});